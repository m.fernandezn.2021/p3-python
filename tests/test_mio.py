# Ejercicio 4 (finished)
import unittest
from mysound import Sound


class TestMio(unittest.TestCase):

    def test_signal(self):
        frequency = 300  # frequency of 330 Hz
        amplitude = 5000  # amplitude of 5000
        sound = Sound(1)  # 1 second
        # Generate sine wave of 330 Hz and 5000 amplitude
        sound.sin(frequency, amplitude)
        self.assertEqual(0, sound.buffer[0])  # Checking values
        self.assertEqual(0, sound.buffer[11025])  # Checking values

        sound = Sound(2)  # 2 seconds sound buffer
        frequency = 500  # frequency of 500 Hz
        amplitude = 700  # amplitude of 700
        # generate a 2 second sinusoidal signal at 500 Hz
        sound.sin(frequency, amplitude)
        self.assertTrue(all(-Sound.max_amplitude <= x <= Sound.max_amplitude
                            for x in sound.buffer))
        # Testing generated sine wave has the expected amplitude
        # Checking values (expected frequency and phase)
        self.assertEqual(0, sound.buffer[22050])
        self.assertEqual(0, sound.buffer[44100])  # Checking values


if __name__ == '__main__':
    unittest.main()

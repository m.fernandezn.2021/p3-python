# Ejercicio 8 (finished)
import unittest
from soundops import soundadd, Sound


class TestSoundAdd(unittest.TestCase):
    newSound1 = Sound(1)
    myNewSoundGenerated1 = newSound1.sin(500, 1000)
    newSound2 = Sound(2)
    myNewSoundGenerated2 = newSound2.sin(1200, 3000)

    def testComparingValues(self):
        # Checking values from buffers
        myResultantSound1 = self.newSound1.sin(550, 8000)
        myResultantSound2 = self.newSound2.sin(660, 8000)
        self.assertEqual(44100, len(myResultantSound1))
        self.assertEqual(88200, len(myResultantSound2))

    def testValuesSumResults(self):
        # Checking buffer values with the resultant buffer
        maxBufferForResultant = max(len(self.newSound1.buffer),
                                    len(self.newSound2.buffer))
        resultantBuffer = soundadd(self.newSound1, self.newSound2)
        self.assertEqual(maxBufferForResultant, len(resultantBuffer.buffer))

    def testingValue(self):
        # Comparing a certain value
        sumOfSounds = soundadd(self.newSound1, self.newSound2)
        valueForTest = self.newSound1.buffer[300] + self.newSound2.buffer[300]
        self.assertTrue(valueForTest, sumOfSounds.buffer[300])


if __name__ == '__main__':
    unittest.main()

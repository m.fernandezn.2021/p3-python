# Ejercicio 6 (finished)
import unittest
from mysoundsin import Sound, SoundSin


class TestSoundSin(unittest.TestCase):
    myNewSound = SoundSin(1, 2000, 500)

    def testSignalValues(self):
        # Checking buffer values and number of samples
        self.assertEqual(len(self.myNewSound.buffer),
                         self.myNewSound.samples_second)

    def testDurationValue(self):
        # Testing duration value
        # myNewSound = SoundSin(1, 2000, 500)
        self.assertEqual(self.myNewSound.durationValue(), 1)

    def testComparingValues(self):
        # Comparing sounds from class Sound and SoundSin
        sound1 = Sound(1)
        soundForComparison = sound1.sin(2000, 500)
        # print(type(soundForComparison), type(self.myNewSound.buffer))
        self.assertTrue(soundForComparison, self.myNewSound.buffer)


if __name__ == '__main__':
    unittest.main()

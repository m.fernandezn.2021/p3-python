# Ejercicio 5 (finished)
from mysound import Sound


class SoundSin(Sound):  # Inherit from Sound class
    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)
        # Call parent class constructor to generate
        # a sine wave with given parameters
        self.sin(frequency, amplitude)  # Generates a sine wave

    def durationValue(self):
        return self.duration

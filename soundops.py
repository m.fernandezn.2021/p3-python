# Ejercicio 7 (finished)
from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:
    # Getting buffers
    bufferSound1 = s1.buffer
    bufferSound2 = s2.buffer
    print(f"Buffer lengths: \n Buffer1: {len(bufferSound1)} \n"
          f" Buffer2: {len(bufferSound2)} \n")

    # Sum of values
#    soundResult = [a + b for a, b in zip(bufferSound1, bufferSound2)]
#    print(f"Buffer result= {len(soundResult)}")

    exitSound = Sound(max(s1.duration, s2.duration))
    for i in range(min(len(s1.buffer), len(s2.buffer))):
        exitSound.buffer[i] = s1.buffer[i] + s2.buffer[i]

    # Comparing values and copying if necessary
    if len(s1.buffer) > len(s2.buffer):
        exitSound.buffer[len(s2.buffer):] = s1.buffer[len(s2.buffer):]
    else:
        exitSound.buffer[len(s1.buffer):] = s2.buffer[len(s1.buffer):]

    return exitSound


# sound1 = Sound(1)  # One second object
# sound1.sin(500, 10000)

# sound2 = Sound(1)  # One second object
# sound2.sin(660, 10000)

# soundadd(sound1, sound2)
